#-------------------------------------------------
#
# Project created by QtCreator 2015-01-08T14:53:51
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ReloadForm
TEMPLATE = app


SOURCES += main.cpp\
        reloadform.cpp \
    editperson.cpp

HEADERS  += reloadform.h \
    editperson.h

FORMS    += reloadform.ui \
    editperson.ui
