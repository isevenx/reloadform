#include "editperson.h"
#include "ui_editperson.h"

editPerson::editPerson(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::editPerson)
{
    ui->setupUi(this);    
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(buttonOK()));
    connect(ui->buttonBox,SIGNAL(rejected()),this,SLOT(hide()));
}
void editPerson::changeFolders(QString str, QDir path){
    curDir = path;
    if (str == "Add"){
        ui->label->setText("Add new folder:");
        ui->lineEdit->setText("");
    }
    else if(str == "Edit"){
        ui->label->setText("Edit person named");
        ui->lineEdit->setText(curDir.dirName());
    }
    else{
        ui->label->setText("Are you sure in deleting this person?");
        ui->lineEdit->setText(curDir.dirName());

    }
}
void editPerson::buttonOK(){
    if(!(ui->lineEdit->text() == "")){
        if(ui->label->text() == "Add new folder:"){
            curDir.mkdir(QString(curDir.absolutePath()+"/"+ui->lineEdit->text()));
        }
        else if(QString(ui->label->text()).startsWith("Edit")){
            personName = curDir.absolutePath();
            personName.chop(curDir.dirName().length());
            curDir.rename(curDir.absolutePath(),QString(personName+ui->lineEdit->text()));
        }
        else{
            curDir.rmdir(curDir.absolutePath());
        }
    }
    this->hide();
}

editPerson::~editPerson()
{
    delete ui;
}
