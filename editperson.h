#ifndef EDITPERSON_H
#define EDITPERSON_H

#include <QMainWindow>
#include <QDebug>
#include <QDir>

namespace Ui {
class editPerson;
}

class editPerson : public QMainWindow
{
    Q_OBJECT

public:
    explicit editPerson(QWidget *parent = 0);
    ~editPerson();
private slots:
    void buttonOK();
    void changeFolders(QString str,QDir path);
private:
    QDir curDir;
    QString personName;
    Ui::editPerson *ui;
};

#endif // EDITPERSON_H
