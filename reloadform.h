#ifndef RELOADFORM_H
#define RELOADFORM_H

#include <QMainWindow>

#include <QTcpSocket>
#include <QTcpServer>
#include <QHostAddress>

#include <QDebug>
#include <QThread>
#include <QProcess>
#include <QCoreApplication>
#include <QFileDialog>
#include <QStringListModel>
#include <QShortcut>

#include <editperson.h>

namespace Ui {
class ReloadForm;
}

class ReloadForm : public QMainWindow
{
    Q_OBJECT

public:
    explicit ReloadForm(QWidget *parent = 0);
    ~ReloadForm();

private slots:
    void reloadScripts();
    void exitProgram();
    void changePath();
    void start_stopButton();
    void onButtonPressA();
    void onButtonPressD();
    void onButtonPressE();
signals:
    void changeFolders(QString str, QDir path);
private:
    Ui::ReloadForm *ui;

    editPerson ep;

    QString dir;

    QTcpSocket socket;
    QTcpSocket *client;
    QTcpServer server;
    QTcpSocket toClassifyFaces;
    QHostAddress hostAdress;
};

#endif // RELOADFORM_H
