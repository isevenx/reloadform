#include "reloadform.h"
#include "ui_reloadform.h"


ReloadForm::ReloadForm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ReloadForm)
{
    ui->setupUi(this);

    QProcess *faceDetect = new QProcess();
    faceDetect->startDetached("/home/rihard/QtPrograms/build-ClassifyFaces/ClassifyFaces");
    faceDetect->waitForStarted(2000);

    QProcess myProcess;
    QStringList params;
    params << "office_check.py";
    myProcess.startDetached("python",params);
    myProcess.waitForFinished(-1);

    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(reloadScripts()));
    connect(ui->pushButton_2,SIGNAL(clicked()),this,SLOT(exitProgram()));
    connect(ui->pushButton_4,SIGNAL(clicked()),this,SLOT(changePath()));
    connect(ui->pushButton_3,SIGNAL(clicked()),this,SLOT(start_stopButton()));
    connect(ui->pushButton_5,SIGNAL(clicked()),this,SLOT(onButtonPressA()));
    connect(ui->pushButton_6,SIGNAL(clicked()),this,SLOT(onButtonPressD()));
    connect(ui->pushButton_7,SIGNAL(clicked()),this,SLOT(onButtonPressE()));
    connect(this,SIGNAL(changeFolders(QString, QDir)),&ep,SLOT(changeFolders(QString, QDir)));

    hostAdress.setAddress("127.0.0.1");

    socket.connectToHost("127.0.0.1",9998,QIODevice::WriteOnly);

    ui->textBrowser->setText("Status");
}

void ReloadForm::reloadScripts()
{  
      if (socket.state())
      {
          ui->textBrowser->setText("\nReloading");

          socket.write("learn");
          socket.waitForBytesWritten(-1);

          QProcess myProcess;
          QStringList params;

          params << "facedetector_0.py";
          myProcess.start("python",params);
          myProcess.waitForFinished(-1);

          QString myP_out = myProcess.readAll();
          ui->textBrowser->setText(myP_out);
          socket.write("Complete");
          socket.waitForBytesWritten(-1);          
      }
      else
      {
          ui->textBrowser->append("no connection with socket");
          ui->textBrowser->append("please, try again later");
      }
}

void ReloadForm::exitProgram()
{
    if (socket.state())
    {
        socket.write("exit");
        socket.waitForBytesWritten(-1);
        QCoreApplication::quit();
    }
}
void  ReloadForm::changePath(){
    dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    "/home",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    QDir directory(dir);
    QStringList stringList = directory.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    ui->listWidget->addItems(stringList);
    ui->lineEdit->setText(dir);
}
void ReloadForm::start_stopButton(){
    if(ui->pushButton_3->text() == "Start"){
        ui->pushButton_3->setText("Stop");
    }
    else{
        ui->pushButton_3->setText("Start");
    }
}
void ReloadForm::onButtonPressA(){
    emit changeFolders("Add",QDir(dir));
    ep.show();
}
void ReloadForm::onButtonPressD(){
    emit changeFolders("Delete",QDir(dir+"/"+ui->listWidget->currentItem()->text()));
    ep.show();
}
void ReloadForm::onButtonPressE(){
    emit changeFolders("Edit",QDir(dir+"/"+ui->listWidget->currentItem()->text()));
    ep.show();
}

ReloadForm::~ReloadForm()
{
    delete ui;
    system("pkill python");
}
